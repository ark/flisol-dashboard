# Run
```bash
export HOST=:6000
export DATABASE_URL=postgres://flisol:flisol@localhost:5432?sslmode=disable
go run main.go
```

## DB migration
```
# Install migrate cmd
go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

# Create migration
migrate create -ext sql -dir db/migrations -seq create_installations_table

# Run migration manually
migrate -database ${DATABASE_URL} -path db/migrations up
```
