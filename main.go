package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "flisol-board.com/m/v2/docs"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/healthcheck"
	"github.com/gofiber/swagger"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/jackc/pgx/v5/stdlib"
)

type Score struct {
	Name     string
	Installs uint8
}

// @title Fiber Example API
// @version 1.0
// @description This is a sample swagger for Fiber
// @contact.name API Support
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @BasePath /
func main() {
	app := fiber.New()

	var host string
	host, ok := os.LookupEnv("HOST")
	if !ok {
		host = ":3000"
	}

	// DATABASE_URL=postgres://flisol:flisol@localhost:5432
	db, err := sql.Open("pgx", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		"file://db/migrations",
		"postgres", driver)
	m.Up()

	app.Use(healthcheck.New(healthcheck.Config{
		LivenessProbe: func(c *fiber.Ctx) bool {
			return true
		},
		ReadinessProbe: func(c *fiber.Ctx) bool {
			fmt.Printf("XYZ %v\n", db)
			return nil != db
		},
	}))

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Get("/:category/dashboard", func(c *fiber.Ctx) error {

		var result []Score
		query := fmt.Sprintf("select name, score from installs where category = '%s'", c.Params("category"))
		rows, err := db.Query(query)
		defer rows.Close()
		if err != nil {
			log.Println(err)
		}
		for rows.Next() {
			var score Score
			rows.Scan(&score.Name, &score.Installs)
			result = append(result, score)
		}
		return c.JSON(result)
	})

	app.Get("/swagger/*", swagger.HandlerDefault) // default

	app.Listen(host)
}

// GetUser example
//
//	@Summary	Get score dashboard of the given software type
//	@Tags		admin
//	@Accept		json
//	@Produce	json
//	@Param		sw_type	path	string	true	"Software type"
//	@Success	200	{object}	Score
//	@Failure	400	{object}	error
//	@Failure	404	{object}	error
//	@Router		/{sw_type}/dashboard [get]
func this(c *fiber.Ctx) error {
	return fiber.NewError(404, "Can not find ID")
}
